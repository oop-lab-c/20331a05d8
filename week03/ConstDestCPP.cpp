//Program to demonstrate the usage of a constructor and destructor in C++
#include<iostream>
using namespace std;
class Student
{
    //private declaration of class variables
    string fName,cName;
    int rNum,cCode;
    double sPercentage;

    //public declaration of member functions
    public:
    //constructor of Student class
    Student(string fullName,int rollNum,double semPercentage,string collegeName, int collegeCode)
    {
        //Assigning the values taken from the user in the main function
        fName=fullName;
        rNum=rollNum;
        sPercentage=semPercentage;
        cName=collegeName;
        cCode=collegeCode;
    }
    //defining a function to display the Student details
    void display()
    {
        cout<<"Name: "<<fName<<endl;
        cout<<"Roll number: "<<rNum<<endl;
        cout<<"Sem percentage: "<<sPercentage<<endl;
        cout<<"college name: "<<cName<<endl;
        cout<<"college code: "<<cCode<<endl;
    }
    //destructor
   ~Student()
   {
       cout<<"Object Destroyed"<<endl;
   };
};
int main()
{
string fullName,collegeName;
int rollNum,collegeCode;
double semPercentage; 
//Taking the user inputs from the console
cout<<"Enter your full name :"<<endl;
cin>>fullName;
cout<<"Enter your roll number :"<<endl;
cin>>rollNum;  
cout<<"Enter your sem percentage :"<<endl;
cin>>semPercentage;  
cout<<"Enter your college name :"<<endl;
cin>>collegeName;  
cout<<"Enter your college code :"<<endl;
cin>>collegeCode;  
//Constructor call of Student class by creating an object
Student obj(fullName,rollNum,semPercentage,collegeName,collegeCode);
obj.display();//Calling display method to display the details of student
return 0;
}