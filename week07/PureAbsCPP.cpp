//Program to demonstrate pure abstraction using virtual functions in C++
#include<iostream>
using namespace std;
class Animal//Pure abstract class
{
    public:
    //Declaration of virtual function by using pure specifier(=0) syntax
    virtual void bark()=0;
    virtual void walk()=0;
};
//Derived class inherited from Base class and implementing the pure virtual functions
class Dog : public Animal
{
    public:
    void bark()
    {
        cout<<"Bow"<<endl;
    }
    void walk()
    {
        cout<<"Walking"<<endl;
    }
};
int main()
{
    Dog obj;
    obj.bark();
    obj.walk();
    return 0;
}