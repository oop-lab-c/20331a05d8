import java.util.*;

class parent {
    void fadd() {
        System.out.println("I'M parent");
    }
}

class child extends parent {
    void fadd(int a, int b) {
        System.out.println(a + b);
    }

    public static void main(String[] args) {
        int a, b;
        child obj = new child();
        System.out.println("ENTER TWO INTEGERS ONE AFTER THE OTHER");
        Scanner sc = new Scanner(System.in);
        a = sc.nextInt();
        b = sc.nextInt();
        obj.fadd(a, b);
        parent objp = new parent();
        objp.fadd();
    }
}