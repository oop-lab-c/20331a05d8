
import java.util.*;

class Shape
{
    int sides()
    {
        return 0;
    }
}
class Square extends Shape
{
    
    int sides()
    {
        return 4;
    }
}
class Triangle extends Shape
{
    
    int sides()
    {
        return 3;
    }
}
class Pentagon extends Shape
{
    
    int sides()
    {
        return 5;
    }
}

class MethodORJava
{
    public static void main(String[] args)
    {
        
        Shape obj=new Shape();
        System.out.println("Number of sides of a circle : "+obj.sides());
        Square obj1=new Square();
        System.out.println("Number of sides of a Square : "+obj1.sides());
        Triangle obj2=new Triangle();
        System.out.println("Number of sides of a Triangle : "+obj2.sides());
        Pentagon obj3=new Pentagon();
        System.out.println("Number of sides of a Pentagon : "+obj3.sides());
    }
}